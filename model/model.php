<?php
	include "connection.php";
class Model{
		protected $db=null;
		function __construct(){
			$this->db = Connection::getInstence();
		}
		public function fetchAll($sql,$object){
			$result  = $this->db->query($sql);
			return $result->fetchAll(PDO::FETCH_CLASS,$object);
		}
		public function query($sql,$data = array()){
			$this->db->prepare($sql)->execute($data);
		}
		public function fetch($sql){
			$result  = $this->db->query($sql);
			return $result->fetch(PDO::FETCH_OBJ);
		}
	}