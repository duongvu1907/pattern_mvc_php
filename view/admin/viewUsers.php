<div class="card">
	<div class="card-header bg-primary">
		<h4>List Users</h4>
	</div>
	<div class="card-body">
		<a href="admin.php?controller=user&action=add" class="btn btn-success">Add</a>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user): ?>
					<tr>
						<td><?php echo $user->getId() ?></td>
						<td><?php echo $user->getName() ?></td>
						<td><?php echo $user->getEmail() ?></td>
						<td>
							<a href="admin.php?controller=user&action=edit&id=<?php echo $user->getId() ?>"><i class="fa fa-edit"></i>Edit</a> &nbsp;&nbsp;&nbsp;
							<a onclick="return confirm('Are you sure ?')" href="admin.php?controller=user&action=delete&id=<?php echo $user->getId() ?>"><i class="fa fa-trash">Delete</i></a>
						</td>
					</tr>	
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>