<div class="card">
	<div class="card-header bg-primary">
		<h4>Edit User</h4>
	</div>
	<div class="card-body">
		<form action="" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						Email : <input type="email" required name="email" <?php if (isset($user->id)) { ?> readonly <?php } ?>  class="form-control" value="<?php echo isset($user->email)?$user->email:'' ?>" >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						Name : <input type="text" class="form-control" name="name" value="<?php echo isset($user->name)?$user->name:''?>" required>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						New Password : <input type="Password" class="form-control" name="password" <?php if (!isset($user->id)) { ?> required <?php } ?> >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<button class="btn btn-success" type="submit">Submit</button>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</form>
	</div>
</div>