<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" href="public/css/bootstrap.min.css">
	<style type="text/css">
		*{
			margin: 0;
			padding: 0;
		}
		.container{
			max-width: 1000px;
		}
		.text-center{
			text-align:center;
		}
		.form-group{
			margin: 20px 0px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="card">
			<div class="card-header text-center">
				<h4>Login</h4>
			</div>
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<label for="email">Email</label>
								<input type="email" name="email" required="" placeholder="Email ***" class="form-control">
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<label for="email">Password</label>
								<input type="password" name="password" required="" placeholder="Password ***" class="form-control">
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<button class="btn btn-primary" type="submit">Login</button>&nbsp;&nbsp;
								<button class="btn btn-danger" type="reset">Reset</button>
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>