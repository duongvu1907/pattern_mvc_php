<!DOCTYPE html>
<html>
<head>
	<title>Manager Orders & Login</title>
	<link rel="stylesheet" href="public/css/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	        
	<style type="text/css">
		.navbar-nav{
			padding-left: 30px;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a class="navbar-brand" href="admin.php">
			<img src="public/images.png" alt="Logo" style="width:40px;">
		</a>
		<ul class="navbar-nav">
			
			<li class="nav-item">
				<a class="nav-link" href="admin.php?controller=order">Orders</a>
			</li>
			<li class="nav-item">
				<a class="nav-link " href="admin.php?controller=user">Users</a>
			</li>
		</ul>
		<ul class="navbar-nav " style="position: absolute;right: 20px;">
			<li>
				<a href="admin.php?controller=middleware&action=logout"><i class="fa fa-sign-out"></i>Logout</a>
			</li>
		</ul>
	</nav>
	<div class="container" style="margin-top: 100px">
		<?= @$content ?>
	</div>
</body>
</html>