<div class="card">
	<div class="card-header bg-primary">
		<h4>Edit order</h4>
	</div>
	<div class="card-body">
		<form action="" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						Ten MH : <input type="text" required name="tenMH"  class="form-control" value="<?php echo isset($order->tenMH)?$order->tenMH:'' ?>" >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<input type="file" name="anhMH" <?php if(!isset($order->id)) echo "required" ?> >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						So don hang <input type="number" class="form-control" name="soDH" value="<?php echo isset($order->soDH)?$order->soDH:'' ?>" required >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						Ten khach hang <input type="text" required class="form-control" name="tenKH" value="<?php echo isset($order->tenKH)?$order->tenKH:'' ?>"  >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						Dia chi: <input type="text" required class="form-control" name="diachiKH" value="<?php echo isset($order->diachiKH)?$order->diachiKH:'' ?>"  >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						Ngay dat: <input type="date" required class="form-control" name="ngaydat" value="<?php echo isset($order->ngaydat)?$order->ngaydat:'' ?>"  > 
						Gia: <input type="text" required class="form-control" name="gia" value="<?php echo isset($order->gia)?$order->gia:'' ?>"  > 
						Trang thai : <input name="trangthai" type="checkbox" <?php if (isset($order->id)&&$order->trangthai==1) {
							echo "checked";
						} ?>>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<button class="btn btn-success" type="submit">Submit</button>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</form>
	</div>
</div>