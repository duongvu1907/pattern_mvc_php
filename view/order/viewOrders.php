<div class="card">
	<div class="card-header">
		<h4>List Order</h4>
	</div>
	<div class="card-body">
		<button class="btn btn-danger"><a href="admin.php?controller=order&action=add">ADD</a></button>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>TenKH</th>
					<th>Dia chi</th>
					<th>TenMH</th>
					<th>AnhMH</th>
					<th>So luong</th>
					<th>Ngay dat</th>
					<th>Gia</th>
					<th>User</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($orders as $order): ?>
					<tr>
						<td><?php echo $order->getId(); ?></td>
						<td><?php echo $order->getTenKH(); ?></td>
						<td><?php echo $order->getDiachiKH(); ?></td>
						<td><?php echo $order->getTenMH(); ?></td>
						<td><img src="public/upload/<?php echo $order->getAnhMH(); ?>" alt="" width="100px"></td>
						<td><?php echo $order->getSoDH(); ?></td>
						<td><?php echo $order->getNgaydat(); ?></td>
						<td><?php echo $order->getGia(); ?></td>
						<td><?php echo $user->name ?></td>
						<td>
							<a href="admin.php?controller=order&action=edit&id=<?php echo $order->getId() ?>"><i class="fa fa-edit"></i>Edit</a> &nbsp;&nbsp;&nbsp;
							<a onclick="return confirm('Are you sure ?')" href="admin.php?controller=order&action=delete&id=<?php echo $order->getId() ?>"><i class="fa fa-trash"></i>Delete</a>
						</td>
						
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>