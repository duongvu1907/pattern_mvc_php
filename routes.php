<?php
	$controllers = array(
		"pages"=>["home","errors"],
		"middleware"=>["login","logout"],
		"user"=>["home","edit","add","delete"],
		"order"=>["home","edit","add","delete"],

	);
	if (!array_key_exists($controller, $controllers) || !in_array($action,$controllers[$controller])) {
		$controller = "user";
		$action = "error";
	}
	include_once "controller/".$controller."_controller.php";
	$kernel = str_replace("_", "", ucwords($controller,"_")."Controller");

	$controller = new $kernel;
	$controller->$action();
