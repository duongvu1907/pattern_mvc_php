<?php 
	/**
	 * 
	 */
	class Order
	{
		protected $id;
		protected $user_id;

		protected $tenKH;

		protected $anhMH;

		protected $soDH;

		protected $tenMH;

		protected $diachiKH;

		protected $trangthai;


		protected $gia;

		protected $ngaydat;
		protected $created_at;
		protected $updated_at;



	
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenKH()
    {
        return $this->tenKH;
    }

    /**
     * @param mixed $tenKH
     *
     * @return self
     */
    public function setTenKH($tenKH)
    {
        $this->tenKH = $tenKH;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnhMH()
    {
        return $this->anhMH;
    }

    /**
     * @param mixed $anhMH
     *
     * @return self
     */
    public function setAnhMH($anhMH)
    {
        $this->anhMH = $anhMH;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSoDH()
    {
        return $this->soDH;
    }

    /**
     * @param mixed $soDH
     *
     * @return self
     */
    public function setSoDH($soDH)
    {
        $this->soDH = $soDH;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenMH()
    {
        return $this->tenMH;
    }

    /**
     * @param mixed $tenMH
     *
     * @return self
     */
    public function setTenMH($tenMH)
    {
        $this->tenMH = $tenMH;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiachiKH()
    {
        return $this->diachiKH;
    }

    /**
     * @param mixed $diachiKH
     *
     * @return self
     */
    public function setDiachiKH($diachiKH)
    {
        $this->diachiKH = $diachiKH;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrangthai()
    {
        return $this->trangthai;
    }

    /**
     * @param mixed $trangthai
     *
     * @return self
     */
    public function setTrangthai($trangthai)
    {
        $this->trangthai = $trangthai;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGia()
    {
        return $this->gia;
    }

    /**
     * @param mixed $gia
     *
     * @return self
     */
    public function setGia($gia)
    {
        $this->gia = $gia;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNgaydat()
    {
        return $this->ngaydat;
    }

    /**
     * @param mixed $ngaydat
     *
     * @return self
     */
    public function setNgaydat($ngaydat)
    {
        $this->ngaydat = $ngaydat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
 ?>