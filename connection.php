<?php

class Connection{
	protected static $instence = null;
	public static function getInstence(){
		try {
			self::$instence = new PDO("mysql:host=127.0.0.1;dbname=lesson","root","");
			self::$instence->exec("SET NAMES 'utf8'");
			self::$instence->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
		 	die($e->getMessage());
		}
		return self::$instence;
	}
} 