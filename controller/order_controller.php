<?php
include "controller/Base_controller.php";
include "model/model.php";
include "Enity/Order.php";

/**
 * 
 */
class OrderController extends BaseController
{
	function __construct(){
		$this->folder = "order";
		$this->model = new Model;
	}
	public function home(){
		$orders = $this->model->fetchAll('select * from orders',"Order");
		$email = $_SESSION["email"];
		$user = $this->model->fetch("select * from users where email='$email'");
		$this->render("viewOrders",array("orders"=>$orders,"user"=>$user));
	}
	public function edit(){
		$id = isset($_GET['id'])?$_GET['id']:0;
		$order = $this->model->fetch("select * from orders where id=$id");
		$this->render("viewEditOrder", array("order"=>$order));
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$id = isset($_GET['id'])?$_GET['id']:0;

			$order = $this->model->fetch("select * from orders where id=$id");

			$tenMH = $_POST["tenMH"];
			$tenKH = $_POST["tenKH"];
			$soDH = $_POST["soDH"];
			$diachiKH = $_POST["diachiKH"];
			$ngaydat = $_POST["ngaydat"];
			$gia = $_POST["gia"];
			$trangthai = $_POST["trangthai"]=="on"?1:0;
			$email = $_SESSION["email"];
			$user = $this->model->fetch("select * from users where email='$email'");
			
			if ($_FILES["anhMH"]["name"]!="") {
					$anh = $_FILES["anhMH"]["name"];
					$anh = time().$_FILES["anhMH"]["name"];
				if ($order->anhMH!="" && file_exists('public/upload/'.$order->anhMH)) {

					unlink('public/upload/'.$order->anhMH);
					
					
				}
					
					move_uploaded_file($_FILES["anhMH"]["tmp_name"], "public/upload/$anh");
				$this->model->query("update orders set anhMH=? where id=?",array($anh,$id));
			}

			$this->model->query("update orders set user_id=?,tenMH=?,tenKH=?,soDH=?,diachiKH=?,ngaydat=?,gia=?,trangthai=? where id=?",array($user->id,$tenMH,$tenKH,$soDH,$diachiKH,$ngaydat,$gia
				,$trangthai,$id));
			header("location:admin.php?controller=order");
		}
	}
	public function add(){
		$this->render("viewEditOrder");
		if ($_SERVER["REQUEST_METHOD"] == "POST") {

			$tenMH = $_POST["tenMH"];
			$tenKH = $_POST["tenKH"];
			$soDH = $_POST["soDH"];
			$diachiKH = $_POST["diachiKH"];
			$ngaydat = $_POST["ngaydat"];
			$gia = $_POST["gia"];

			$anh = $_FILES["anhMH"]["name"];
			$anh = time().$_FILES["anhMH"]["name"];
			move_uploaded_file($_FILES["anhMH"]["tmp_name"], "public/upload/$anh");

			$trangthai = $_POST["trangthai"]=="on"?1:0;
			$email = $_SESSION["email"];
			$user = $this->model->fetch("select * from users where email='$email'");
			$this->model->query("insert into orders(tenMH,tenKH,soDH,diachiKH,ngaydat,gia,trangthai,user_id,anhMH) values(?,?,?,?,?,?,?,?,?)",array($tenMH,$tenKH,$soDH,$diachiKH,$ngaydat,$gia,$trangthai,$user->id,$anh));
			header("location:admin.php?controller=order");
		}
	}
	public function delete(){
		$id = isset($_GET['id'])?$_GET['id']:0;
		$this->model->query("delete from orders where id=?",array($id));
		header("location:admin.php?controller=order");
	}
}