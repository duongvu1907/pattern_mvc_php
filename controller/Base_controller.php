<?php

class BaseController{
	protected $folder;
	protected $model;
	public function render($view,$data = []){
		$view = "view/".$this->folder."/".$view.".php";
		if (file_exists($view)) {
			extract($data);
			ob_start();
			require_once $view;
			$content  = ob_get_contents();
			ob_clean();
			ob_end_flush();
			if(isset($_SESSION['email'])){
				require_once "view/layout.php";
			}else{
				echo $content;
			}
		}else{
			header("Location : index.php?controller='pages'&action='error'");
		}

	}
}