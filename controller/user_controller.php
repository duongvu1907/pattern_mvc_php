<?php 
include "controller/Base_controller.php";
include "model/model.php";
include "Enity/User.php";
/**
 * 
 */
class UserController extends BaseController
{
	function __construct(){
		$this->folder = "admin";
		$this->model = new Model;
	}
	public function home(){
		$users = $this->model->fetchAll("Select * from users","User");
		$this->render("viewUsers",array("users"=>$users));
	}
	public function edit(){
		$id = isset($_GET['id'])?$_GET['id']:0;
		$user = $this->model->fetch("select * from users where id=$id");
		$this->render("viewEditUsers", array("user"=>$user));
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$email = $_POST["email"];
			$name = $_POST["name"];
			$password = md5($_POST["password"]);
			var_dump($_POST);
			if ($password=="") {
				$this->model->query("Update users set email=?,name=? where email=?",array($email,$name,$email));
			}else{
				$this->model->query("Update users set email=?,name=?,password=? where email=?",array($email,$name,$password,$email));
			}
			header('Location:admin.php?controller=user');
		}
	}
	public function add(){
		$this->render("viewEditUsers");
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$email = $_POST["email"];
			$name = $_POST["name"];
			$password = md5($_POST["password"]);
			$this->model->query("insert into users(name,email,password) value(?,?,?)",array($name,$email,$password));
			header('Location:admin.php?controller=user');
		}
	}
	public function delete(){
		$id = isset($_GET['id'])?$_GET['id']:0;
		$this->model->query("delete from users where id=?",array($id));
		header('Location:admin.php?controller=user');
	}
	public function error(){
		$this->render("error");
	}
}