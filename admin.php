<?php
	session_start();
	$controller = null;
	$action = null;
	if(isset($_SESSION["email"])  == false){
		$controller = "middleware";
		$action = "login";
	}else{
		if (isset($_GET["controller"])){
			$controller = $_GET["controller"];

			if (isset($_GET["action"])){
				$action = $_GET["action"];
			}else{
				$action = "home";
			}
		
		}else{
			$controller = "user";
			$action = "home";
		}
	}
	include "routes.php";
